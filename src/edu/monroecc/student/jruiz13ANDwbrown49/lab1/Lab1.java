/* Written by Fig and Wyatt J. Brown.
 * Due date: 2015-10-07
 * Description: A program that implements and tests a class that represents
 * one-variable quadratic expressions. */
package edu.monroecc.student.jruiz13ANDwbrown49.lab1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/** The class {@code Lab1} reads input data from a file in order to run a
 * {@code QuadTest}.
 *
 * @author Fig
 * @author Wyatt J. Brown
 * @see QuadTest */
public class Lab1
{
	/** Reads data from the input file and passes it to a QuadTest object.
	 *
	 * @param args an array of Strings representing the command line arguments.
	 * @throws IOException if the input file can not be read.
	 * @see QuadTest#run */
	public static void main(final String[] args) throws IOException
	{
		final QuadTest quadTest = new QuadTest();
		// Open the file and begin processing as input.
		final BufferedReader bufferedReader =
				new BufferedReader(new FileReader("input.txt"));
		for (String line = bufferedReader.readLine(); line != null; line =
				bufferedReader.readLine())// Read file line by line.
			quadTest.run(line);

		bufferedReader.close();// Close the file.
		System.exit(0);// Exit cleanly.
	}
}
