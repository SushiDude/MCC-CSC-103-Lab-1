/* Written by Fig and Wyatt J. Brown.
 * Due date: 2015-10-07
 * Description: A program that implements and tests a class that represents
 * one-variable quadratic expressions. */
package edu.monroecc.student.jruiz13ANDwbrown49.lab1;

/** The class {@code Quadratic} represents a one-variable quadratic expression.
 *
 * @author Fig
 * @author Wyatt J. Brown */
public class Quadratic implements Cloneable
{
	/** The return value is the quadratic expression obtained by multiplying
	 * each of q's coefficients by the number r.
	 *
	 * @param r a double to multiply each of q's coefficients by.
	 * @param q a Quadratic object to be scaled by r.
	 * @return a Quadratic object obtained by multiplying each of q's
	 *         coefficients by the number r. */
	public static Quadratic scale(final double r, final Quadratic q)
	{
		return new Quadratic(q.getCoefA() * r, q.getCoefB() * r,
				q.getCoefC() * r);
	}

	/** The return value is the quadratic expression obtained by adding q1 and
	 * q2. For example, the c coefficient of the return value is the sum of q1's
	 * c coefficient and q2's c coefficient.
	 *
	 * @param q1 a Quadratic object.
	 * @param q2 a Quadratic object.
	 * @return a Quadratic object that is the sum of the two that were
	 *         provided. */
	public static Quadratic sum(final Quadratic q1, final Quadratic q2)
	{
		return new Quadratic(q1.getCoefA() + q2.getCoefA(),
				q1.getCoefB() + q2.getCoefB(), q1.getCoefC() + q2.getCoefC());
	}

	/* A one-variable quadratic expression is an arithmetic expression of the
	 * form ax^2+bx+c , where a, b, and c are some fixed numbers (called the
	 * coefficients). */
	private final double coefA;
	private final double coefB;
	private final double coefC;

	/** Construct a new Quadratic with all three coefficients set to zero. */
	public Quadratic()
	{
		coefA = 0.0;
		coefB = 0.0;
		coefC = 0.0;
	}

	/** Construct a new Quadratic and set the three coefficients to the supplied
	 * values.
	 *
	 * @param coefA a double representing first coefficient.
	 * @param coefB a double representing second coefficient.
	 * @param coefC a double representing third coefficient. */
	public Quadratic(final double coefA, final double coefB, final double coefC)
	{
		this.coefA = coefA;
		this.coefB = coefB;
		this.coefC = coefC;
	}

	/** Creates and returns a copy of this object.
	 *
	 * @return object of the class Quadratic that is a clone of this
	 *         instance. */
	@Override
	public Object clone()
	{
		Quadratic quadratic;
		try
		{
			quadratic = (Quadratic) super.clone();
		} catch (final CloneNotSupportedException e)
		{
			throw new RuntimeException(
					"This class does not implement Cloneable.");
		}
		return quadratic;
	}

	/* FIXME: warning| [overrides] Class Quadratic overrides equals, but neither
	 * it nor any superclass overrides hashCode method. */
	/** Indicates whether some other object is "equal to" this one.
	 *
	 * @param object the reference object with which to compare.
	 * @return {@code true} if this object is the same as the object argument;
	 *         {@code false} otherwise. */
	@Override
	public boolean equals(final Object object)
	{
		if (object instanceof Quadratic)
		{
			final Quadratic quadratic = (Quadratic) object;
			return coefA == quadratic.coefA && coefB == quadratic.coefB
					&& coefC == quadratic.coefC;
		} else
			return false;
	}

	/** Returns the value of this Quadratic object when evaluated with the
	 * supplied value for x.
	 *
	 * @param x a double representing x in the expression ax^2+bx+c.
	 * @return a double representing value of when evaluated with the supplied
	 *         value for x */
	public double evalExpression(final double x)
	{
		return coefA * x * x + coefB * x + coefC;
	}

	/** Returns the first coefficient in this Quadratic object.
	 *
	 * @return a double representing the first coefficient. */
	public double getCoefA()
	{
		return coefA;
	}

	/** Returns the second coefficient in this Quadratic object.
	 *
	 * @return a double representing the second coefficient. */
	public double getCoefB()
	{
		return coefB;
	}

	/** Returns the third coefficient in this Quadratic object.
	 *
	 * @return a double representing the third coefficient. */
	public double getCoefC()
	{
		return coefC;
	}

	/** Returns the number of real roots of this Quadratic object. This answer
	 * could be 0, 1, 2, or infinity. In the case of an infinite number of real
	 * roots, the method returns 3.
	 *
	 * @return an int representing the number real roots. In the case of an
	 *         infinite number of real roots, the result 3. */
	public int getRootNum()
	{
		// There are six rules for finding the real roots of a quadratic
		// expression:
		// If a, b, and c are all zero, then every value of x is a real root.
		if (coefA == 0 && coefB == 0 && coefC == 0)
			return 3;
		// If a and b are zero but c is nonzero, then there are no real roots.
		else if (coefA == 0 && coefB == 0 && coefC != 0)
			return 0;
		// If a is zero and b is nonzero, then the only real root is x = -c/b .
		else if (coefA == 0 && coefB != 0)
			return 1;
		// If a is nonzero and b^2<4ac , then there are no real roots.
		else if (coefA != 0 && coefB * coefB < 4.0 * coefA * coefC)
			return 0;
		// If a is nonzero and b^2 = 4ac , then there is one real root x =
		// -b/2a .
		else if (coefA != 0 && coefB * coefB == 4.0 * coefA * coefC)
			return 1;
		// If a is nonzero and b^2>4ac , then there are two real roots.
		else if (coefA != 0 && coefB * coefB > 4.0 * coefA * coefC)
			return 2;
		// Return nonzero error code. This should never happen.
		else
			return -1;
	}

	/** Returns the smaller of the two real roots of this Quadratic object. In
	 * the case of an infinite number of real roots, the method returns 0.
	 *
	 * @return a double representing the smaller of the two real roots. In the
	 *         case of an infinite number of real roots, the result 0. */
	public double getRootOne()
	{
		switch (getRootNum())
		{
		case 1:
			return coefA == 0.0 ? -coefC / coefB : -coefB / (2 * coefA);
		case 2:
			final double rootOne =
					((-coefB) - Math.sqrt(coefB * coefB - 4.0 * coefA * coefC))
							/ (2.0 * coefA);
			final double rootTwo =
					(-coefB + Math.sqrt(coefB * coefB - 4.0 * coefA * coefC))
							/ (2.0 * coefA);
			return rootOne < rootTwo ? rootOne : rootTwo;
		case 3:
			return 0;// If every value of x is a real root, then return zero.
		}
		return -1;// Return nonzero error code. This should never happen.
	}

	/** Returns the greater of the two real roots of this Quadratic object. In
	 * the case of an infinite number of real roots, the method returns 0.
	 *
	 * @return a double representing the greater of the two real roots. In the
	 *         case of an infinite number of real roots, the result 0. */
	public double getRootTwo()
	{
		switch (getRootNum())
		{
		case 1:
			return coefA == 0.0 ? -coefC / coefB : -coefB / (2 * coefA);
		case 2:
			final double rootOne =
					((-coefB) - Math.sqrt(coefB * coefB - 4.0 * coefA * coefC))
							/ (2.0 * coefA);
			final double rootTwo =
					(-coefB + Math.sqrt(coefB * coefB - 4.0 * coefA * coefC))
							/ (2.0 * coefA);
			return rootOne > rootTwo ? rootOne : rootTwo;
		case 3:
			return 0;// If every value of x is a real root, then return zero.
		}
		return -1;// Return nonzero error code. This should never happen.
	}
}
