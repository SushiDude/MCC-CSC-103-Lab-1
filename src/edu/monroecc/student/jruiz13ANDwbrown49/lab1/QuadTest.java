/* Written by Fig and Wyatt J. Brown.
 * Due date: 2015-10-07
 * Description: A program that implements and tests a class that represents
 * one-variable quadratic expressions. */
package edu.monroecc.student.jruiz13ANDwbrown49.lab1;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/** The class {@code QuadTest} implements methods for testing the correctness of
 * the {@code Quadratic} class.
 *
 * @author Fig
 * @author Wyatt J. Brown
 * @see Quadratic */
public class QuadTest
{
	/* FIXME: This should not be static. However, due to the specification,
	 * there can not be any more instance variables. This could lead to
	 * undesirable test numbers in the introduction when there are multiple
	 * QuadTest objects.
	 *
	 * @see #intro() @see #run(String) */
	private static int testNumber = 0;

	/* This class will have only three objects of the class quadratic (for the
	 * first quadratic, the second quadratic, and the quadratic which is the sum
	 * of the two.) */
	private Quadratic q1;
	private Quadratic q2;
	private Quadratic qS;

	/** Construct a new QuadTest with all three Quadratics set to null. */
	public QuadTest()
	{
		q1 = null;
		q2 = null;
		qS = null;
	}

	/** Construct a new QuadTest and set the three Quadratics to the supplied
	 * values.
	 *
	 * @param q1 a Quadratic object representing the first quadratic.
	 * @param q2 a Quadratic object representing the second quadratic.
	 * @param qS a Quadratic object representing the sum of the first and second
	 *        quadratic. */
	public QuadTest(final Quadratic q1, final Quadratic q2, final Quadratic qS)
	{
		this.q1 = q1;
		this.q2 = q2;
		this.qS = qS;
	}

	/** This will do the required calculations to create a clone of the first
	 * quadratic, check if they are aliases, then check if the two quadratics
	 * have the same coefficients.
	 *
	 * @param dubArray an array of eight doubles: three numbers for the three
	 *        coefficients, one number for the value of x, the value of the
	 *        number to scale the quadratic expression, and the next three
	 *        numbers are the coefficients for the second quadratic.
	 * @see #output(double[], Quadratic, Quadratic, boolean, boolean) */
	public void calculate(final double[] dubArray)
	{
		q1 = new Quadratic(dubArray[0], dubArray[1], dubArray[2]);
		q2 = new Quadratic(dubArray[5], dubArray[6], dubArray[7]);
		qS = Quadratic.sum(q1, q2);

		final Quadratic clonedQuadratic = (Quadratic) q1.clone();

		// Output the results.
		output(dubArray, Quadratic.scale(dubArray[4], q1), clonedQuadratic,
				clonedQuadratic == q1, clonedQuadratic.equals(q1));
	}

	/** Outputs a description of what the program is doing to the standard
	 * output stream.
	 *
	 * @see #output(double[], Quadratic, Quadratic, boolean, boolean) */
	public void intro()
	{
		System.out.println("\n*********************************" + "\nTest # "
				+ ++testNumber + "\n-------------");
	}

	/** Output the results of the calculations to the standard output stream.
	 *
	 * @param dubArray an array of eight doubles: three numbers for the three
	 *        coefficients, one number for the value of x, the value of the
	 *        number to scale the quadratic expression, and the next three
	 *        numbers are the coefficients for the second quadratic.
	 * @param scaled a Quadratic object that has been scaled by the scaler
	 *        value.
	 * @param clone a Quadratic object that is a clone of the first.
	 * @param alias a boolean representing whether the first Quadratic and the
	 *        clone are aliases or not.
	 * @param equal a boolean representing whether the first Quadratic and the
	 *        clone are equal or not.
	 * @see #calculate(double[]) */
	public void output(final double[] dubArray, final Quadratic scaled,
			final Quadratic clone, final boolean alias, final boolean equal)
	{
		final double x = dubArray[3];
		final double scaler = dubArray[4];

		System.out.println("\nThe first quadratic is:");
		System.out.println("\t" + q1.getCoefA() + "*x*x+" + q1.getCoefB()
				+ "*x+" + q1.getCoefC());
		System.out.print("The values of the first quadratic expression with x="
				+ x + " is: \t" + q1.evalExpression(x) + "\n\n");

		System.out.print("The first quadratic after scaling with R=" + scaler
				+ ": \n\t");
		System.out.print(scaled.getCoefA() + "*x*x + " + scaled.getCoefB()
				+ "*x + " + scaled.getCoefC());

		System.out.print("\n\nNumber of roots:\t");
		switch (scaled.getRootNum())
		{
		case 0:
			System.out.print("Zero");
			break;
		case 1:
			System.out.println("One");
			System.out.print("\tValue of root: \t" + scaled.getRootOne());
			break;
		case 2:
			System.out.println("Two");
			System.out.println("Value of Root One: \t" + scaled.getRootOne());
			System.out.println("Value of Root Two: \t" + scaled.getRootTwo());
			break;
		case 3:
			System.out.println("Infinity");
			break;
		}

		System.out.println("\nThe second quadratic is:");
		System.out.println("\t" + q2.getCoefA() + "*x*x+" + q2.getCoefB()
				+ "*x+" + q2.getCoefC());

		System.out.println("\nThe quadratic which is the sum of the first and"
				+ " second quadratics:");
		System.out.println("\t" + qS.getCoefA() + "*x*x+" + qS.getCoefB()
				+ "*x+" + qS.getCoefC());

		System.out.println("\nThe clone of the first quadratic is:");
		System.out.println("\t" + clone.getCoefA() + "*x*x+" + clone.getCoefB()
				+ "*x+" + clone.getCoefC());

		if (!alias && equal)
			System.out.println("The first quadratic and the clone are not"
					+ " aliases, but are equal to eachother.");
	}

	/** Returns an array representing the tokenized version of the parameter.
	 *
	 * @param qString a String of eight doubles: three numbers for the three
	 *        coefficients, one number for the value of x, the value of the
	 *        number to scale the quadratic expression, and the next three
	 *        numbers are the coefficients for the second quadratic.
	 * @return an array of doubles representing the tokenized version of the
	 *         String parameter. */
	public double[] parse(final String qString)
	{
		final StringTokenizer tokenizer = new StringTokenizer(qString);
		final double[] dubArray = new double[8];
		try
		{
			for (int element = 0; element < dubArray.length; ++element)
				dubArray[element] = Double.parseDouble(tokenizer.nextToken());
			return dubArray;
		} catch (final NoSuchElementException exception)
		{
			System.err.println(
					"\nNot enough data. Line ignored: " + qString + "\n");
		}
		return null;
	}

	/** Prints the introduction, calculates, then outputs the results of the
	 * test to the standard output stream.
	 *
	 * @param qString a String of eight doubles: three numbers for the three
	 *        coefficients, one number for the value of x, the value of the
	 *        number to scale the quadratic expression, and the next three
	 *        numbers are the coefficients for the second quadratic.
	 * @see #intro()
	 * @see #calculate(double[]) */
	public void run(final String qString)
	{
		final double[] dubArray = parse(qString);
		if (dubArray != null)
		{
			intro();
			calculate(dubArray);
		}
	}
}
