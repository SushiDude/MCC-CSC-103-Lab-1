*********************************
Test # 1
-------------

The first quadratic is:
	2.0*x*x+8.0*x+6.0
The values of the first quadratic expression with x=3.0 is: 	48.0

The first quadratic after scaling with R=2.0: 
	4.0*x*x + 16.0*x + 12.0

Number of roots:	Two
Value of Root One: 	-3.0
Value of Root Two: 	-1.0

The second quadratic is:
	1.0*x*x+1.0*x+1.0

The quadratic which is the sum of the first and second quadratics:
	3.0*x*x+9.0*x+7.0

The clone of the first quadratic is:
	2.0*x*x+8.0*x+6.0
The first quadratic and the clone are not aliases, but are equal to eachother.

*********************************
Test # 2
-------------

The first quadratic is:
	0.0*x*x+0.0*x+0.0
The values of the first quadratic expression with x=1.0 is: 	0.0

The first quadratic after scaling with R=4.0: 
	0.0*x*x + 0.0*x + 0.0

Number of roots:	Infinity

The second quadratic is:
	2.0*x*x+2.0*x+2.0

The quadratic which is the sum of the first and second quadratics:
	2.0*x*x+2.0*x+2.0

The clone of the first quadratic is:
	0.0*x*x+0.0*x+0.0
The first quadratic and the clone are not aliases, but are equal to eachother.

*********************************
Test # 3
-------------

The first quadratic is:
	0.0*x*x+0.0*x+5.0
The values of the first quadratic expression with x=2.0 is: 	5.0

The first quadratic after scaling with R=5.0: 
	0.0*x*x + 0.0*x + 25.0

Number of roots:	Zero
The second quadratic is:
	0.0*x*x+1.0*x+2.0

The quadratic which is the sum of the first and second quadratics:
	0.0*x*x+1.0*x+7.0

The clone of the first quadratic is:
	0.0*x*x+0.0*x+5.0
The first quadratic and the clone are not aliases, but are equal to eachother.

Not enough data. Line ignored: 4	10	7

*********************************
Test # 4
-------------

The first quadratic is:
	0.0*x*x+2.0*x+6.0
The values of the first quadratic expression with x=3.0 is: 	12.0

The first quadratic after scaling with R=2.0: 
	0.0*x*x + 4.0*x + 12.0

Number of roots:	One
	Value of root: 	-3.0
The second quadratic is:
	1.0*x*x+0.0*x+2.0

The quadratic which is the sum of the first and second quadratics:
	1.0*x*x+2.0*x+8.0

The clone of the first quadratic is:
	0.0*x*x+2.0*x+6.0
The first quadratic and the clone are not aliases, but are equal to eachother.

*********************************
Test # 5
-------------

The first quadratic is:
	2.0*x*x+4.0*x+9.0
The values of the first quadratic expression with x=1.0 is: 	15.0

The first quadratic after scaling with R=5.0: 
	10.0*x*x + 20.0*x + 45.0

Number of roots:	Zero
The second quadratic is:
	1.0*x*x+2.0*x+0.0

The quadratic which is the sum of the first and second quadratics:
	3.0*x*x+6.0*x+9.0

The clone of the first quadratic is:
	2.0*x*x+4.0*x+9.0
The first quadratic and the clone are not aliases, but are equal to eachother.

*********************************
Test # 6
-------------

The first quadratic is:
	3.0*x*x+6.0*x+3.0
The values of the first quadratic expression with x=5.0 is: 	108.0

The first quadratic after scaling with R=2.0: 
	6.0*x*x + 12.0*x + 6.0

Number of roots:	One
	Value of root: 	-1.0
The second quadratic is:
	5.0*x*x+5.0*x+5.0

The quadratic which is the sum of the first and second quadratics:
	8.0*x*x+11.0*x+8.0

The clone of the first quadratic is:
	3.0*x*x+6.0*x+3.0
The first quadratic and the clone are not aliases, but are equal to eachother.
